package jwt_authen

import (
	"github.com/dgrijalva/jwt-go"
)

type UserClaimsPayload struct {
	UserId string `json:"user_id"`
}

type jwtClaims struct {
	UserClaimsPayload
	jwt.StandardClaims
}

package jwt_authen

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/suite"

	"book/logger"
)

type JwtTestSuite struct {
	suite.Suite
	j         *jwtService
	token     string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (suite *JwtTestSuite) SetupTest() {
	suite.T().Log("init Jwt Service")

	// Init Config
	viper.AddConfigPath("../config")
	viper.SetConfigName("config_test")

	// Read Config
	if err := viper.ReadInConfig(); err != nil {
		fmt.Fprintf(os.Stderr, "unable to read config: %v\n", err)
		os.Exit(1)
	}

	logger.InitLogger()
	InitConfig()
	service, err := New(context.Background(), uuid.NewString())
	if err != nil {
		suite.FailNowf("cannot get new jwt service because: %v", err.Error())
	}

	createAt, err := time.Parse(time.RFC3339, "0001-01-01T00:00:00Z")
	if err != nil {
		suite.FailNowf("cannot createAt because: %v", err.Error())
	}
	suite.CreatedAt = createAt

	updateAt, _ := time.Parse(time.RFC3339, "0001-01-01T00:00:00Z")
	if err != nil {
		suite.FailNowf("cannot updateAt because: %v", err.Error())
	}
	suite.UpdatedAt = updateAt

	suite.j = &service
}

func (suite *JwtTestSuite) TestGenerateToken() {
	token, err := suite.j.GenerateToken(
		context.Background(), UserClaimsPayload{
			UserId: "1",
		},
	)

	suite.NotNil(token)
	suite.Nil(err)
	suite.T().Log(token)
	suite.token = token

}

func (suite *JwtTestSuite) TestVerifyJWTToken() {
	// suite.token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMSIsImltZWkiOlt7IklEIjoxLCJVc2VySUQiOjEsIlNlcmlhbE5vIjoiMTIzNDU2NzgiLCJJbWVpIjoiODc2NTQzMjEiLCJDcmVhdGVkQXQiOiIwMDAxLTAxLTAxVDAwOjAwOjAwWiIsIlVwZGF0ZWRBdCI6IjAwMDEtMDEtMDFUMDA6MDA6MDBaIiwiRmNtVG9rZW4iOm51bGx9XSwidXNlcl9ubyI6IjEyMzQ1NjciLCJleHAiOjIwMjQxOTkzMTAsImlhdCI6MTY2NDIwMjkxMCwiaXNzIjoiQkZfS0JKIn0.Z9v0C5C5nU1MqMtOCFfAqCeeg6aKSl34YafhruNvOwsFAVVIJUaenrGejKtnE7NgavC4jRwf3kBnBziKlwBP8-i8FuFFzXyqrd145rcjwH_VcQYE7gIz6il2AHr0aZ3WEg5M-hlfsSSXFt7lUk4nODxFTtzGSbiFjfAnKp4fsY0fltgj826KAEoLhQYgkDBcIjh1zXkrYOHhVuwREw6ddjLgKVd_Q3quaimaGAXTZl_bEYXxYCmzSd_IPwQTtJwskVUZyk8gPzzubNqTWc7Gk7OxPzn5mHFPpT2MynoXj4F3E3aipu6chqULJT8BZeDpe1cDL9RzioR1b_snHlsxdg"
	jwtClaim, err := suite.j.VerifyJWToken(context.Background(), "Bearer "+suite.token)
	suite.NotNil(jwtClaim)
	suite.Nil(err)
	j := &jwtClaims{
		UserClaimsPayload: UserClaimsPayload{
			UserId: "1",
		},
	}
	suite.Equal(j.UserId, jwtClaim.UserId)

}

func (suite *JwtTestSuite) TestGenerateTokenError() {

	token, err := suite.j.GenerateToken(context.Background(), UserClaimsPayload{
		UserId: "1",
	})

	suite.NotNil(err)
	suite.Equal("", token)

}

func (suite *JwtTestSuite) TestVerifyTokenError() {
	jwtClaim, err := suite.j.VerifyJWToken(context.Background(), "Bearer "+suite.token)
	suite.NotNil(err)
	suite.Equal(jwtClaim, jwtClaim)

}

func (suite *JwtTestSuite) TestVerifyTokenEmptyError() {
	suite.token = ""
	jwtClaim, err := suite.j.VerifyJWToken(context.Background(), "Bearer "+suite.token)
	suite.NotNil(err)
	suite.Equal(jwtClaim, jwtClaim)

}

func TestJwtTestSuite(t *testing.T) {
	suite.Run(t, new(JwtTestSuite))
}

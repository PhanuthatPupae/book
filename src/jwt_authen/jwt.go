package jwt_authen

import (
	"context"
	"time"

	"github.com/dgrijalva/jwt-go"

	"book/utils"
)

func (s *jwtService) GenerateToken(ctx context.Context, userClaimsPayload UserClaimsPayload) (string, error) {
	now := time.Now()
	expiresAt := now.Add(s.config.ExpiresIn)
	claims := &jwtClaims{
		UserClaimsPayload: UserClaimsPayload{
			UserId: userClaimsPayload.UserId,
		},
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiresAt.Unix(),
			Issuer:    s.config.TokenIssuer,
			IssuedAt:  now.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	dir := s.config.JWTPrivateKeyPath
	privateKey, err := utils.ReadRSAPrivateKey(dir)
	if err != nil {
		s.log.Errorf("cannot read RSA private key from %s because: %v", dir, err)
		return "", err
	}
	t, err := token.SignedString(privateKey)
	if err != nil {
		s.log.Errorf("cannot sign string with private key because: %v", err)
		return t, err
	}

	return t, nil
}

func (s *jwtService) VerifyJWToken(ctx context.Context, tokenString string) (jwtClaims, error) {
	tokenString = utils.GetTokenStringFromBearerPrefix(tokenString)

	dir := s.config.JWTPublicKeyPath
	publicKey, err := utils.ReadRSAPublicKey(dir)
	if err != nil {
		s.log.Errorf("can not read RSA Public key from %s because: %v", dir, err)
		return jwtClaims{}, err
	}

	claims := jwtClaims{}
	_, err = jwt.ParseWithClaims(tokenString, &claims, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})
	if err != nil {
		s.log.Errorf("cannot parse with claims because: %v", err)
		return claims, err
	}

	s.log.Debugf("token of userId: %s is valid", claims.UserId)
	return claims, nil
}

package repository

import (
	"gorm.io/gorm"

	"book/db/postgresql"
	"book/model"
)

func (repo Repository) GetTagList(tx *gorm.DB) ([]model.Tag, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	result := []model.Tag{}
	err := tx.Model(&model.Tag{}).Find(&result).Error
	if err != nil {
		return result, err
	}
	return result, nil
}

func (repo Repository) GetTag(tx *gorm.DB, tag model.Tag) (model.Tag, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	result := model.Tag{}
	err := tx.Model(&model.Tag{}).Where(&tag).Find(&result).Error
	if err != nil {
		return result, err
	}
	return result, nil
}

func (repo Repository) CreateTag(tx *gorm.DB, tag model.Tag) (model.Tag, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	err := tx.Model(&model.Tag{}).Create(&tag).Error
	if err != nil {
		return tag, err
	}
	return tag, nil
}

func (repo Repository) UpdateTag(tx *gorm.DB, tag model.Tag) (model.Tag, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	err := tx.Model(&model.Tag{}).Updates(&tag).Error
	if err != nil {
		return tag, err
	}
	return tag, nil
}

func (repo Repository) DeleteTag(tx *gorm.DB, tag model.Tag) (model.Tag, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	err := tx.Model(&model.Tag{}).Delete("id", tag.ID).Error
	if err != nil {
		return tag, err
	}
	return tag, nil
}

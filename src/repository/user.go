package repository

import (
	"gorm.io/gorm"

	"book/db/postgresql"
	"book/model"
)

func (repo Repository) GetUserList(tx *gorm.DB) ([]model.User, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	result := []model.User{}
	err := tx.Model(&model.User{}).Find(&result).Error
	if err != nil {
		return result, err
	}
	return result, nil
}

func (repo Repository) GetUser(tx *gorm.DB, user model.User) (model.User, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	result := model.User{}
	err := tx.Model(&model.User{}).Where(&user).Find(&result).Error
	if err != nil {
		return result, err
	}
	return result, nil
}

func (repo Repository) CreateUser(tx *gorm.DB, user model.User) (model.User, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	err := tx.Model(&model.User{}).Create(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func (repo Repository) UpdateUser(tx *gorm.DB, user model.User) (model.User, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	err := tx.Model(&model.User{}).Updates(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func (repo Repository) DeleteUser(tx *gorm.DB, user model.User) (model.User, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	err := tx.Model(&model.User{}).Delete("id", user.ID).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

package repository

import (
	"gorm.io/gorm"

	"book/db/postgresql"
	"book/model"
)

func (repo Repository) GetBookList(tx *gorm.DB) ([]model.Book, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	result := []model.Book{}
	err := tx.Model(&model.Book{}).Find(&result).Error
	if err != nil {
		return result, err
	}
	return result, nil
}

func (repo Repository) GetBook(tx *gorm.DB, book model.Book) (model.Book, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	result := model.Book{}
	err := tx.Model(&model.Book{}).Where(&book).Find(&result).Error
	if err != nil {
		return result, err
	}
	return result, nil
}

func (repo Repository) CreateBook(tx *gorm.DB, book model.Book) (model.Book, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	err := tx.Model(&model.Book{}).Create(&book).Error
	if err != nil {
		return book, err
	}
	return book, nil
}

func (repo Repository) UpdateBook(tx *gorm.DB, book model.Book) (model.Book, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	err := tx.Debug().Model(&model.Book{}).Where("id = ?", book.ID).Updates(&book).Error
	if err != nil {
		return book, err
	}
	return book, nil
}

func (repo Repository) DeleteBook(tx *gorm.DB, book model.Book) (model.Book, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	err := tx.Model(&model.Book{}).Delete("id", book.ID).Error
	if err != nil {
		return book, err
	}
	return book, nil
}

func (repo Repository) CreateBookTag(tx *gorm.DB, bookTag model.BookTag) (model.BookTag, error) {
	if tx == nil {
		tx = postgresql.GormDB
	}
	err := tx.Model(&model.BookTag{}).Create(&bookTag).Error
	if err != nil {
		return bookTag, err
	}
	return bookTag, nil
}

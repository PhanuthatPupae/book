package model

import (
	"time"

	"gorm.io/gorm"
)

type Book struct {
	ID                uint            `gorm:"primarykey" json:"id,omitempty"`
	CreatedAt         time.Time       `json:"created_at,omitempty" gorm:"type:timestamptz"`
	UpdatedAt         time.Time       `json:"updated_at,omitempty" gorm:"type:timestamptz;autoUpdateTime"`
	DeletedAt         *gorm.DeletedAt `gorm:"index" json:"deleted_at,omitempty"`
	Title             string          `json:"title"`
	Author            string          `json:"author"`
	YearOfPublication time.Time       `json:"year_of_publication"`
	UserID            uint            `json:"user_id"`
	TagID             uint            `json:"tag_id" gorm:"-"`
}

type BookTag struct {
	TagId  uint `json:"tag_id"`
	BookId uint `json:"book_id"`
}

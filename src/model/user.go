package model

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	ID        uint           `gorm:"primarykey" json:"id"`
	CreatedAt time.Time      `json:"created_at" gorm:"type:timestamptz"`
	UpdatedAt time.Time      `json:"updated_at" gorm:"type:timestamptz;autoUpdateTime"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at,omitempty"`
	IsActive  bool           `json:"is_active"`
	Username  string         `json:"username"`
	Password  string         `json:"password"`
}

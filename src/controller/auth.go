package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"book/global_variable"
	"book/interface/http/response"
	service "book/services"
)

func Login(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to Login")
	responseOutput := response.ResponseOutput{}

	input := service.LoginInput{}
	if err := context.ShouldBind(&input); err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	requestId, _ := context.Get(global_variable.KEY_REQUEST_ID)
	requestIdStr := requestId.(string)

	serviceObj := service.New(requestIdStr)
	output, err := serviceObj.Login(input)
	if err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	responseOutput.Message = "success"
	responseOutput.Data = output
	context.JSON(http.StatusOK, responseOutput)
}

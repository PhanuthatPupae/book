package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"book/global_variable"
	"book/interface/http/response"
	service "book/services"
	"book/utils"
)

func CreateBook(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to CreateBook")
	responseOutput := response.ResponseOutput{}

	input := service.CreateBookInput{}
	if err := context.ShouldBind(&input); err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	requestId, _ := context.Get(global_variable.KEY_REQUEST_ID)
	requestIdStr := requestId.(string)

	serviceBook := service.New(requestIdStr)
	output, err := serviceBook.CreateBook(input)
	if err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	responseOutput.Message = "success"
	responseOutput.Data = output
	context.JSON(http.StatusOK, responseOutput)
}

func GetBookList(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to GetBookList")
	responseOutput := response.ResponseOutput{}
	input := service.GetBookListInput{}

	requestId, _ := context.Get(global_variable.KEY_REQUEST_ID)
	requestIdStr := requestId.(string)

	serviceBook := service.New(requestIdStr)
	output, err := serviceBook.GetBookList(input)
	if err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}
	responseOutput.Message = "success"
	responseOutput.Data = output
	context.JSON(http.StatusOK, responseOutput)
}

func GetBookByID(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to GetBookByID")
	responseOutput := response.ResponseOutput{}
	input := service.GetBookByIdInput{}

	input.BookId = utils.StringToUint(context.Param("id"))

	requestId, _ := context.Get(global_variable.KEY_REQUEST_ID)
	requestIdStr := requestId.(string)

	serviceBook := service.New(requestIdStr)
	output, err := serviceBook.GetBookById(input)
	if err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	responseOutput.Message = "success"
	responseOutput.Data = output
	context.JSON(http.StatusOK, responseOutput)
}

func UpdateBookByID(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to UpdateBookByID")

	input := service.UpdateBookInput{}

	responseOutput := response.ResponseOutput{}

	if err := context.ShouldBind(&input); err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	input.ID = utils.StringToUint(context.Param("id"))
	requestId, _ := context.Get(global_variable.KEY_REQUEST_ID)
	requestIdStr := requestId.(string)

	serviceBook := service.New(requestIdStr)
	output, err := serviceBook.UpdateBook(input)
	if err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	responseOutput.Message = "success"
	responseOutput.Data = output
	context.JSON(http.StatusOK, responseOutput)
}

func DeleteBookByID(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to DeleteBookByID")
	responseOutput := response.ResponseOutput{}

	input := service.DeletetBookInput{}
	input.BookId = utils.StringToUint(context.Param("id"))

	requestId, _ := context.Get(global_variable.KEY_REQUEST_ID)
	requestIdStr := requestId.(string)

	serviceBook := service.New(requestIdStr)
	output, err := serviceBook.DeletetBook(input)
	if err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	responseOutput.Message = "success"
	responseOutput.Data = output

	context.JSON(http.StatusOK, responseOutput)
}

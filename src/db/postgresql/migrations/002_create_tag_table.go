package migrations

import (
	"github.com/pkg/errors"
	"gorm.io/gorm"
)

var CreateTagTable = &Migration{
	Number: 2,
	Name:   "create tags table",
	Forwards: func(db *gorm.DB) error {
		const sql = `
		CREATE TABLE tags (
   			id BIGSERIAL PRIMARY KEY,
    		code VARCHAR(255) NOT NULL,
    		name VARCHAR(255) NOT NULL,
    		is_active BOOLEAN NOT NULL
		);
		`

		err := db.Exec(sql).Error
		if err != nil {
			return errors.Wrap(err, "unable to create tags table")
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, CreateTagTable)
}

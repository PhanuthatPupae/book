package migrations

import (
	"github.com/pkg/errors"
	"gorm.io/gorm"
)

var CreateBookTable = &Migration{
	Number: 3,
	Name:   "create books table",
	Forwards: func(db *gorm.DB) error {
		const sql = `
		CREATE TABLE books (
    		id BIGSERIAL PRIMARY KEY,
    		created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
   		 	updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    		deleted_at TIMESTAMPTZ DEFAULT NULL,
    		title VARCHAR(255) NOT NULL,
    		author VARCHAR(255) NOT NULL,
    		year_of_publication TIMESTAMPTZ,
    		user_id INT NOT NULL,
			CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(id)
		);
		`

		err := db.Exec(sql).Error
		if err != nil {
			return errors.Wrap(err, "unable to create books table")
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, CreateBookTable)
}

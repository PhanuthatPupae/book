package migrations

import (
	"errors"
	"fmt"
	"sort"

	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"book/logger"
)

type Migration struct {
	Number uint `gorm:"primary_key"`
	Name   string

	Forwards func(db *gorm.DB) error `gorm:"-"`
}

var Migrations []*Migration

func Migrate(number int) error {
	dbHost := viper.GetString("Database.Host")
	dbPort := viper.GetString("Database.Port")
	dbUser := viper.GetString("Database.Username")

	if dbUser == "" {
		dbUser = "postgres"
	}

	dbPassword := viper.GetString("Database.Password")
	if dbPassword == "" {
		dbPassword = "postgres"
	}

	dbName := viper.GetString("Database.DatabaseName")
	dbSchema := viper.GetString("Database.DatabaseSchema")

	migrationIDs := make(map[uint]struct{})
	for _, migration := range Migrations {
		if _, ok := migrationIDs[migration.Number]; ok {
			err := fmt.Errorf("duplicate migration Number found: %d", migration.Number)
			logger.Logger.Errorf("Unable to apply migrations, err: %+v", err)
			return err
		}

		migrationIDs[migration.Number] = struct{}{}
	}

	sort.Slice(Migrations, func(i, j int) bool {
		return Migrations[i].Number < Migrations[j].Number
	})

	connStr := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s search_path=%s sslmode=disable TimeZone=%s",
		dbHost,
		dbPort,
		dbUser,
		dbPassword,
		dbName,
		dbSchema,
		"Asia/Bangkok",
	)

	db, err := gorm.Open(postgres.Open(connStr), &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: false,
	})
	if err != nil {
		logger.Logger.Errorf("unable to connect db: %+v", err)
		return err
	}

	if dbSchema != "" {
		if err := db.Exec(fmt.Sprintf(`CREATE SCHEMA IF NOT EXISTS %v`, dbSchema)).Error; err != nil {
			return err
		}
		if err := db.Exec(fmt.Sprintf(`SET search_path='%v'`, dbSchema)).Error; err != nil {
			return err
		}
	}

	logger.Logger.Debugf("ensuring migrations table is present")
	if err := db.AutoMigrate(&Migration{}); err != nil {
		return err
	}

	if number == -1 {
		number = int(Migrations[len(Migrations)-1].Number)
	}

	for _, migration := range Migrations {
		if migration.Number > uint(number) {
			break
		}
		migrate := Migration{}
		err := db.Where("number = ?", migration.Number).First(&migrate).Error
		if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
			logger.Logger.Errorf("unable to find latest migration")
			continue
		}
		if migrate.Number == migration.Number {
			logger.Logger.Infof("skip migration number: %d", migration.Number)
			continue
		}

		migrationLogger := logger.Logger.With(
			"migration_number", migration.Number,
		)

		migrationLogger.Infof("applying migration %q", migration.Name)

		tx := db.Begin()

		if err := migration.Forwards(tx); err != nil {
			logger.Logger.Errorf("unable to apply migration, rolling back. err: %+v", err)
			if err := tx.Rollback().Error; err != nil {
				logger.Logger.Errorf("unable to rollback... err: %+v", err)
			}

			continue

		}

		if err := tx.Commit().Error; err != nil {
			logger.Logger.Errorf("unable to commit transaction... err: %+v", err)
		}

		if err := db.Create(migration).Error; err != nil {
			logger.Logger.Errorf("unable to create migration record. err: %+v", err)

		}

	}

	return nil
}

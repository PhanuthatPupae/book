package migrations

import (
	"github.com/pkg/errors"
	"gorm.io/gorm"
)

var CreateBookTagTable = &Migration{
	Number: 4,
	Name:   "create book_tag table",
	Forwards: func(db *gorm.DB) error {
		const sql = `
		CREATE TABLE book_tags (
   			tag_id INT NOT NULL,
    		book_id INT NOT NULL,
    		PRIMARY KEY (tag_id, book_id),
    		CONSTRAINT fk_tag FOREIGN KEY (tag_id) REFERENCES tags(id),
    		CONSTRAINT fk_book FOREIGN KEY (book_id) REFERENCES books(id)
		);

		`

		err := db.Exec(sql).Error
		if err != nil {
			return errors.Wrap(err, "unable to create book_tag table")
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, CreateBookTagTable)
}

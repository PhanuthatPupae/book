package migrations

import (
	"github.com/pkg/errors"
	"gorm.io/gorm"
)

var CreateUserTable = &Migration{
	Number: 1,
	Name:   "create users table",
	Forwards: func(db *gorm.DB) error {
		const sql = `
		CREATE TABLE users (
   		 	id BIGSERIAL PRIMARY KEY,
    		created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    		updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    		deleted_at TIMESTAMPTZ,
    		is_active BOOLEAN NOT NULL,
    		username VARCHAR(255) NOT NULL UNIQUE,
    		password VARCHAR(255) NOT NULL
		);

		`

		err := db.Exec(sql).Error
		if err != nil {
			return errors.Wrap(err, "unable to create users table")
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, CreateUserTable)
}

package migrations

import (
	"github.com/pkg/errors"
	"gorm.io/gorm"
)

var InsertTagTable = &Migration{
	Number: 5,
	Name:   "create book_tag table",
	Forwards: func(db *gorm.DB) error {
		const sql = `
		INSERT INTO tags (code,name,is_active) VALUES
	 	('TAG_01','business',true),
	 	('TAG_02','novel',true);


		`

		err := db.Exec(sql).Error
		if err != nil {
			return errors.Wrap(err, "unable to insert tag table")
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, InsertTagTable)
}

package postgresql

import (
	"book/logger"
	"fmt"
	"os"

	"github.com/jackc/pgx/v4"
	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var GormDB *gorm.DB

func getDbLogLevel(level string) pgx.LogLevel {
	switch level {
	case "info":
		return pgx.LogLevelInfo
	case "warn":
		return pgx.LogLevelWarn
	case "debug":
		return pgx.LogLevelDebug
	case "error":
		return pgx.LogLevelError
	case "trace":
		return pgx.LogLevelTrace
	case "none":
		return pgx.LogLevelNone
	default:
		return pgx.LogLevelInfo
	}
}

func InitGormDB(isTest bool) {
	userName := viper.GetString("Database.Username")
	password := viper.GetString("Database.Password")
	host := viper.GetString("Database.Host")
	port := viper.GetInt("Database.Port")
	databaseName := viper.GetString("Database.DatabaseName")
	databaseSchema := viper.GetString("Database.DatabaseSchema")
	if isTest {
		databaseSchema = viper.GetString("Database.DatabaseTestSchema")
	}

	connStr := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable search_path=%v",
		host,
		port,
		userName,
		password,
		databaseName,
		databaseSchema,
	)

	db, err := gorm.Open(postgres.Open(connStr), &gorm.Config{
		CreateBatchSize: 500,
	})
	if err != nil {
		logger.Logger.Errorf("Unable to connect to database: %s\n", err)
		os.Exit(1)
	}

	sqlDB, _ := db.DB()
	sqlDB.SetMaxIdleConns(viper.GetInt("Database.MaxConnection"))
	sqlDB.SetMaxOpenConns(viper.GetInt("Database.MaxConnection"))

	GormDB = db
}

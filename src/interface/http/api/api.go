package api

import (
	"github.com/gin-gonic/gin"

	"book/interface/http/api/book"
)

func AddRoute(engine *gin.Engine) {
	apiRoute := engine.Group("/api")
	book.AddRoute(apiRoute)
}

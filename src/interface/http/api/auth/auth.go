package auth

import "book/controller"

func init() {
	methodRoutes[ROUTE_POST]["/login"] = controller.Login
}

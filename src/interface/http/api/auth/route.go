package auth

import (
	"github.com/gin-gonic/gin"
)

const (
	ROUTE_GET  = "GET"
	ROUTE_POST = "POST"
)

var methodRoutes = map[string]map[string]gin.HandlerFunc{
	ROUTE_GET:  make(map[string]gin.HandlerFunc),
	ROUTE_POST: make(map[string]gin.HandlerFunc),
}

func AddRoute(router *gin.RouterGroup) {

	for method, routes := range methodRoutes {
		switch method {
		case ROUTE_GET:
			for routeName, routeFunc := range routes {
				router.GET(routeName, routeFunc)
			}
		case ROUTE_POST:
			for routeName, routeFunc := range routes {
				router.POST(routeName, routeFunc)
			}
		}

	}
}

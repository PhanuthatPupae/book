package book

import (
	"github.com/gin-gonic/gin"
)

const (
	ROUTE_GET    = "GET"
	ROUTE_POST   = "POST"
	ROUTE_DELETE = "DELETE"
	ROUTE_PUT    = "PUT"
)

var methodRoutes = map[string]map[string]gin.HandlerFunc{
	ROUTE_GET:    make(map[string]gin.HandlerFunc),
	ROUTE_POST:   make(map[string]gin.HandlerFunc),
	ROUTE_DELETE: make(map[string]gin.HandlerFunc),
	ROUTE_PUT:    make(map[string]gin.HandlerFunc),
}

func AddRoute(router *gin.RouterGroup) {

	for method, routes := range methodRoutes {
		switch method {
		case ROUTE_GET:
			for routeName, routeFunc := range routes {
				router.GET(routeName, routeFunc)
			}
		case ROUTE_POST:
			for routeName, routeFunc := range routes {
				router.POST(routeName, routeFunc)
			}
		case ROUTE_DELETE:
			for routeName, routeFunc := range routes {
				router.DELETE(routeName, routeFunc)
			}
		case ROUTE_PUT:
			for routeName, routeFunc := range routes {
				router.PUT(routeName, routeFunc)
			}
		}

	}
}

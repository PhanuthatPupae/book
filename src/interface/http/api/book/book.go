package book

import "book/controller"

func init() {
	methodRoutes[ROUTE_GET]["/books"] = controller.GetBookList
	methodRoutes[ROUTE_GET]["/books/:id"] = controller.GetBookByID
	methodRoutes[ROUTE_POST]["/books"] = controller.CreateBook
	methodRoutes[ROUTE_PUT]["/books/:id"] = controller.UpdateBookByID
	methodRoutes[ROUTE_DELETE]["/books/:id"] = controller.DeleteBookByID

}

package response

import (
	"book/custom_error"
	"net/http"
)

type ResponseOutput struct {
	Code    int32       `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func ResponseError(err error) (ResponseOutput, int) {
	response := ResponseOutput{}
	httpStatus := http.StatusOK
	response.Data = make(map[string]string)
	response.Message = err.Error()
	response.Code = custom_error.BadRequest
	customError, ok := err.(custom_error.CustomError)
	if ok {
		response.Code = customError.Code
		response.Message = customError.Message
		httpStatus = int(response.Code)
	}

	return response, httpStatus
}

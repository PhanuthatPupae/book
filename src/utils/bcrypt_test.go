package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHashPassword(t *testing.T) {
	t.Run("Test Hash Password Success", func(t *testing.T) {
		password := "123456789"
		hashPassword, err := HashPassword(password)
		assert.NotEmpty(t, hashPassword)
		assert.Nil(t, err)
	})
}

func TestCheckPasswordHash(t *testing.T) {
	t.Run("Test Check Password Success", func(t *testing.T) {
		password := "123456789"
		hashPassword, err := HashPassword(password)
		assert.NotEmpty(t, hashPassword)
		assert.Nil(t, err)
		match := CheckPasswordHash(password, hashPassword)
		assert.Equal(t, true, match)
	})
}

package utils

import "strings"

func GetTokenStringFromBearerPrefix(tokenWithBearer string) string {
	comps := strings.Split(tokenWithBearer, " ")
	if len(comps) != 2 {
		return tokenWithBearer
	}
	if strings.ToLower(comps[0]) != "bearer" {
		return tokenWithBearer
	}
	return comps[1]
}

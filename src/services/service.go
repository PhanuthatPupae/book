package service

import (
	"go.uber.org/zap"

	"book/logger"
	"book/repository"
)

type Service struct {
	RequestId string
	Logger    *zap.SugaredLogger
	Db        repository.Repository
}

func New(requestId string) Service {
	serviceObj := Service{
		RequestId: requestId,
	}

	serviceObj.Logger = logger.Logger.With(
		"request_id", requestId,
		"part", "service",
	)

	return serviceObj
}

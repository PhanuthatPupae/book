package service

import (
	"book/model"
	"book/repository"
	"book/utils"
)

type CreateUserInput struct {
	model.User
}

type CreateUserOutput struct {
}

func (service Service) CreateUser(input CreateUserInput) error {
	service.Logger.Infof("start CreateUser")
	repo := repository.New(service.RequestId)
	service.Logger.Infof("start repo")

	passwordHash, err := utils.HashPassword(input.Password)
	if err != nil {
		service.Logger.Errorf("HashPassword failed cuz: %v", err)
		return err
	}
	service.Logger.Infof(" repo CreateUser")

	input.Password = passwordHash
	_, err = repo.CreateUser(nil, input.User)
	if err != nil {
		service.Logger.Errorf("CreateUser failed cuz: %v", err)
		return err
	}
	return nil

}

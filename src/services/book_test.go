package service_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"book/model"
	service "book/services"
)

func TestCreateBook(t *testing.T) {
	requestId := "requestId_TestCreateDltFirst"
	serviceObj := service.New(requestId)

	t.Run("Create Book Happy", func(t *testing.T) {
		bookResult, err := serviceObj.CreateBook(service.CreateBookInput{
			Book: model.Book{
				Title:             "test",
				Author:            "test",
				UserID:            1,
				YearOfPublication: time.Now(),
			},
		})
		assert.Nil(t, err)
		assert.NotEmpty(t, bookResult.BookId)
		assert.NotEmpty(t, bookResult.TagId)

	})

}

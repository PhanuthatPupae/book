package service

import (
	"context"
	"strconv"

	"book/jwt_authen"
	"book/model"
	"book/repository"
	"book/utils"
)

type LoginInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginOutput struct {
	Token string `json:"token"`
}

func (service Service) Login(input LoginInput) (*LoginOutput, error) {
	service.Logger.Infof("start Login")
	output := &LoginOutput{}

	repo := repository.New(service.RequestId)
	user, err := repo.GetUser(nil, model.User{Username: input.Username})
	if err != nil {
		return nil, err
	}

	match := utils.CheckPasswordHash(input.Password, user.Password)
	if match {
		jwtAuth, err := jwt_authen.New(context.Background(), service.RequestId)
		if err != nil {
			service.Logger.Errorf("jwt_authen.New error: %s", err.Error())
			return nil, err
		}

		token, err := jwtAuth.GenerateToken(context.Background(), jwt_authen.UserClaimsPayload{
			UserId: strconv.FormatUint(uint64(user.ID), 10),
		})
		if err != nil {
			service.Logger.Errorf("jwtAuth.GenerateToken error: %s", err.Error())
			return nil, err
		}
		output.Token = token
	}

	service.Logger.Infof("Login Done")
	return output, nil
}

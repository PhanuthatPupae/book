package service

import (
	"flag"
	"fmt"
	"os"
	"testing"

	"github.com/spf13/viper"

	"book/db/postgresql"
	"book/logger"
)

func TestMain(m *testing.M) {

	configPath := flag.String("config", "", "Path to the config file")
	flag.Parse()

	// Check if the config path is provided
	if GetStringValue(configPath) != "" {
		viper.SetConfigFile(*configPath)
	} else {
		viper.AddConfigPath("../config")
		viper.SetConfigName("config")
	}

	// Read Config
	if err := viper.ReadInConfig(); err != nil {
		fmt.Fprintf(os.Stderr, "unable to read config: %v\n", err)
		os.Exit(1)
	}

	// Init Logger
	logger.InitLogger()

	logger.Logger.Infof("Using config file: %s\n", viper.ConfigFileUsed())

	code, err := run(m)
	if err != nil {
		fmt.Fprintf(os.Stderr, "unable to run test: %v\n", err)
	}
	os.Exit(code)
}

func run(m *testing.M) (code int, err error) {
	// Init Test Database
	postgresql.InitGormDB(false)

	return m.Run(), nil
}

func GetStringValue(s *string) string {
	var val string

	if s != nil {
		val = *s
	}

	return val
}

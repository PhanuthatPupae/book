package service

import (
	"book/db/postgresql"
	"book/model"
	"book/repository"
)

type CreateBookInput struct {
	model.Book
}

type CreateBookOutput struct {
	BookId uint `json:"book_id"`
	TagId  uint `json:"tag_id"`
}

func (service Service) CreateBook(input CreateBookInput) (*CreateBookOutput, error) {
	service.Logger.Infof("start CreateBook")
	output := &CreateBookOutput{}

	repo := repository.New(service.RequestId)

	tx := postgresql.GormDB.Begin()
	if tx.Error != nil {
		service.Logger.Errorf("tx with error: %s", tx.Error.Error())
		return nil, tx.Error
	}
	defer tx.Rollback()

	book, err := repo.CreateBook(
		tx,
		model.Book{
			Title:             input.Title,
			Author:            input.Author,
			YearOfPublication: input.YearOfPublication,
			UserID:            input.UserID,
		})
	if err != nil {
		service.Logger.Infof("CreateBook fail error:%s", err.Error())
		return output, err
	}

	tag, err := repo.GetTag(
		tx,
		model.Tag{
			ID: input.TagID,
		})
	if err != nil {
		service.Logger.Infof("GetTag fail error:%s", err.Error())
		return output, err
	}

	_, err = repo.CreateBookTag(
		tx,
		model.BookTag{
			TagId:  tag.ID,
			BookId: book.ID,
		})
	if err != nil {
		service.Logger.Infof("CreateBookTag fail error:%s", err.Error())
		return output, err
	}

	err = tx.Commit().Error
	if err != nil {
		service.Logger.Errorf("transaction commit failed cuz: %s", err)
		return output, err
	}

	output.BookId = book.ID
	output.TagId = tag.ID
	service.Logger.Infof("CreateBook Done")
	return output, nil
}

type GetBookByIdInput struct {
	BookId uint
}

type GetBookByIdOutput struct {
	model.Book
}

func (service Service) GetBookById(input GetBookByIdInput) (*GetBookByIdOutput, error) {
	service.Logger.Infof("start GetBookById")
	output := &GetBookByIdOutput{}

	repo := repository.New(service.RequestId)

	book, err := repo.GetBook(
		nil,
		model.Book{
			ID: input.BookId,
		})
	if err != nil {
		service.Logger.Infof("GetBook fail error:%s", err.Error())
		return output, err
	}

	output.Book = book
	service.Logger.Infof("GetBookById Done")

	return output, nil
}

type UpdateBookInput struct {
	model.Book
}

type UpdateBookOutput struct {
	model.Book
}

func (service Service) UpdateBook(input UpdateBookInput) (*UpdateBookOutput, error) {
	service.Logger.Infof("start UpdateBook")
	output := &UpdateBookOutput{}

	repo := repository.New(service.RequestId)

	book, err := repo.UpdateBook(nil, input.Book)

	if err != nil {
		service.Logger.Infof("UpdateBook fail error:%s", err.Error())
		return output, err
	}

	output.Book = book
	service.Logger.Infof("UpdateBook Done")

	return output, nil
}

type GetBookListInput struct {
}

type GetBookListOutput []model.Book

func (service Service) GetBookList(input GetBookListInput) (*GetBookListOutput, error) {
	service.Logger.Infof("start GetBookList")
	output := &GetBookListOutput{}

	repo := repository.New(service.RequestId)

	bookList, err := repo.GetBookList(nil)

	if err != nil {
		service.Logger.Infof("GetBookList fail error:%s", err.Error())
		return output, err
	}

	*output = append(*output, bookList...)
	service.Logger.Infof("GetBookList Done")

	return output, nil
}

type DeletetBookInput struct {
	BookId uint
}

type DeletetBookOutput struct {
	BookId uint `json:"book_id" `
}

func (service Service) DeletetBook(input DeletetBookInput) (*DeletetBookOutput, error) {
	service.Logger.Infof("start DeletetBook")
	output := &DeletetBookOutput{}

	repo := repository.New(service.RequestId)

	book, err := repo.DeleteBook(nil, model.Book{ID: input.BookId})

	if err != nil {
		service.Logger.Infof("GetBookList fail error:%s", err.Error())
		return output, err
	}
	output.BookId = book.ID
	service.Logger.Infof("DeletetBook Done")
	return output, nil
}

package cmd

import (
	"book/db/postgresql"
	"book/interface/http"
	"book/logger"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/shopspring/decimal"
	"github.com/spf13/viper"
)

// Global Variable
var wg sync.WaitGroup
var configFile string
var enableDatabase bool
var enableInterface bool

func initConfig() {
	// Init viper
	if configFile != "" {
		viper.SetConfigFile(configFile)
	} else {
		viper.AddConfigPath("./config")
		viper.AddConfigPath("..")
		viper.AddConfigPath(".")
		viper.AddConfigPath("/cfg")
		viper.AddConfigPath("../cfg")
		viper.SetConfigName("config")
	}

	// Read Config
	if err := viper.ReadInConfig(); err != nil {
		fmt.Fprintf(os.Stderr, "unable to read config: %v\n", err)
		os.Exit(1)
	}

	// Set Component Enable Flag
	enableInterface = viper.GetBool("Interface.Enable")
	enableDatabase = viper.GetBool("Database.Enable")

	// Parse Json Decimal to Float
	decimal.MarshalJSONWithoutQuotes = true
}

func initPostgresql() {
	wg.Add(1)
	postgresql.InitGormDB(false)

}

func shutdownPostgresql() {
	postgresql.InitGormDB(false)
	wg.Done()
}

func initListenInterface() {
	wg.Add(1)
	go func() {
		http.InitHttpServer()
		wg.Done()
	}()
}

func initListenOsSignal() {
	wg.Add(1)
	go func() {
		var count int
		chanOsSignal := make(chan os.Signal, 2)
		signal.Notify(chanOsSignal, syscall.SIGTERM, os.Interrupt)

		go func() {
			// Wait Os Signal
			for getSignal := range chanOsSignal {
				// Shutdown if Interrupt or SigTerm Signal is received
				if getSignal == os.Interrupt || getSignal == syscall.SIGTERM {
					count++
					// Get Twice Signal Force Exit without Waiting Close all Components
					if count == 2 {
						logger.Logger.Info("Forcefully exiting")
						os.Exit(1)
					}

					go func() {
						if enableDatabase {
							shutdownPostgresql()
						}
					}()

					go func() {
						if enableInterface {
							http.ShutdownHttpServer()
						}
					}()

					logger.Logger.Info("Signal SIGKILL caught. shutting down")
					logger.Logger.Info("Catching SIGKILL one more time will forcefully exit")

					wg.Done()
				}
			}
			close(chanOsSignal)
		}()
	}()
}

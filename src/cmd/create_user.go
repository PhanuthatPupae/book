package cmd

import (
	"github.com/spf13/cobra"

	"book/db/postgresql"
	"book/logger"
	"book/model"
	service "book/services"
)

var CreateUserCmd = &cobra.Command{
	Use:   "create_user",
	Short: "create_user",
	Run: func(cmd *cobra.Command, args []string) {
		// Init Logger
		logger.InitLogger()
		postgresql.InitGormDB(false)

		serviceObj := service.New("create_user")
		err := serviceObj.CreateUser(service.CreateUserInput{
			User: model.User{
				Username: "admin",
				Password: "password",
				IsActive: true,
			},
		})

		if err != nil {
			serviceObj.Logger.Errorf("createUser failed cuz: %v", err)
		}

	},
}

func init() {
	rootCmd.AddCommand(CreateUserCmd)
}

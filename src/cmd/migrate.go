package cmd

import (
	"book/db/postgresql"
	"github.com/spf13/cobra"

	migration "book/db/postgresql/migrations"
	"book/logger"
)

var MigrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "migrate",
	Run: func(cmd *cobra.Command, args []string) {
		// Init Logger
		logger.InitLogger()

		migration.Migrate(-1)

		postgresql.InitGormDB(false)
		logger.SyncLogger()
	},
}

func init() {
	rootCmd.AddCommand(MigrateCmd)
}

package cmd

import (
	"book/logger"

	"github.com/spf13/cobra"
)

var WorkFlowCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start Master Data Service",
	Run: func(cmd *cobra.Command, args []string) {
		// Init Logger
		logger.InitLogger()

		// Init Listen OS Signal
		initListenOsSignal()

		// Init Database
		if enableDatabase {
			initPostgresql()
		}

		// Init Interface
		if enableInterface {
			initListenInterface()
		}

		// Waiting for Component Shut Down
		wg.Wait()

		// Flush Log
		logger.SyncLogger()
	},
}

func init() {
	rootCmd.AddCommand(WorkFlowCmd)
}

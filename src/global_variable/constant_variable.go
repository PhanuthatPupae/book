package global_variable

const (
	RESULT_SUCCESS = "success"

	KEY_LOGGER      = "logger"
	KEY_REQUEST_ID  = "request_id"
	KEY_USER_ID     = "user_id"
	KEY_MERCHANT_ID = "merchant_id"
	KEY_PART        = "part"

	STATUS_ACTIVE   = "active"
	STATUS_INACTIVE = "inactive"
	STATUS_DELETE   = "delete"

	ACTION_CREATE = "create"
	ACTION_UPDATE = "update"
	ACTION_DELETE = "delete"
	ACTION_LIST   = "list"
	ACTION_GET    = "get"
)

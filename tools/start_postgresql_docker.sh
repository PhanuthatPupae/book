#!/bin/sh

docker run \
    --rm \
    -d \
    -p 5432:5432 \
    -e TZ=Asia/Bangkok \
    -e POSTGRES_PASSWORD=postgres \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_DB=book \
    --name book-pg postgres:14-alpine

#!/bin/sh

docker exec -i book-pg psql -U postgres -c "drop database if exists book" &&
docker exec -i book-pg psql -U postgres -c "create database book"


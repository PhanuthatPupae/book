# Golang Book (Backend)

## Getting Started

### Run Development Server

1. Start PostgreSQL docker

```sh
./tools/start_postgresql_docker.sh  
```

2. Create Database

```sh
./tools/reset_db.sh    
```

3. Migration Table 

```sh
./tools/migrate.sh 
```

3. Init Demo user

```sh
./tools/create_user.sh 
```

4. Start HTTP Server default config `config.yaml`

```sh
./tools/serve.sh 
```


## Project Structure

The structure can be separated to 3 layers:

- Data access layer

  The application may have multiple interfaces to data store using different protocol and driver/library, e.g., PostgerSQL, MySQL, MongoDB. Separating this layer can be benificial when there's a need to support multiple data store types.

  Example module: `db`

- App/Business logic layer

  Contains all functions to perform application logic.

  Example module: `service`

- Interface layer

  Example module: `interface/http`

### Ignore Jwt Token set `DisableAuth: false`  on `config.yaml`

#### Province PostMan Collecttion

`file_name: docs/REST API basics- Book.postman_collection.json`


